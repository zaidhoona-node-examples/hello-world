#Hello World in Nodejs

The project is a basic nodejs example. In the project we print `Hello World!` to the console. 
You can start executing the project by executing `npm start` or `node src/index.js`.

>Note: You need Nodejs pre installed